package main

//go:generate go run gen.go

import (
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/getlantern/systray"

	log "github.com/sirupsen/logrus"
)

// QuitChan - The global kill switch
var QuitChan chan os.Signal

func main() {

	QuitChan = make(chan os.Signal, 1)
	signal.Notify(QuitChan, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	if !isRoot() {
		runAsRoot()
		systray.Quit()
		return
	}

	log.WithFields(log.Fields{"version": buildVersion}).Info("Starting")

	systray.Run(trayReady, trayExit)

	<-QuitChan
	switchBlocking(false)
	log.Info("Shutting down. Bye")
}

func switchBlocking(block bool) {
	hosts := readHosts()
	hosts = manipulateLines(hosts, block)
	writeHosts(hosts)
}

func manipulateLines(hosts []string, block bool) []string {
	for i, line := range hosts {
		line = strings.TrimSpace(line)
		isComment := strings.Index(line, "#") == 0
		isDistractifryEntry := strings.Contains(line, "# distractifry")

		if isDistractifryEntry {
			log.WithFields(log.Fields{"block": block, "isComment": isComment}).Info("Distractifry line")
			if block && isComment {
				line = line[1:]
			} else if !block && !isComment {
				line = fmt.Sprintf("%s %s", "#", line)
			}
			line = strings.TrimSpace(line)
		}

		hosts[i] = line
	}

	return hosts
}
