package main

import (
	"bufio"
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"
)

func readHosts() []string {
	path := getHostsPath()

	file, err := os.Open(path)
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Fatal("Failed opening hosts file")
		return nil
	}

	defer file.Close()

	lines := []string{}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.WithFields(log.Fields{"error": err}).Fatal("Failed reading hosts file")
		return nil
	}

	return lines
}

func writeHosts(hosts []string) {
	path := getHostsPath()

	hostString := ""
	for _, line := range hosts {
		hostString += line + lineBreak
	}

	err := ioutil.WriteFile(path, []byte(hostString), 0644)
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Fatal("Failed writing hosts file")
		return
	}
}
