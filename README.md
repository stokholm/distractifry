# Distractifry

Fry distracting websites using hosts entries.

Distractifry will read your `/etc/hosts` file and toggle any entries that ends with a comment `# distractifry` on and off when you ask it to.

Set up your blocked sites manually in the hosts file and give them the comment so Distractifry can fry the DNS to those domains.

## Configuration

Add entries to your `/etc/hosts` file with the comment `# distractifry` at the end of the entries. Example:

```
# Distractifry entries
127.0.0.1   facebook.com # distractifry
127.0.0.1   news.ycombinator.com # distractifry
```

Distractifry will now toggle these on and off based on the action you select in the menu.

## Running

Distractifry works on MacOS and Windows. Maybe on Linux (untested).

### Windows

Launch the .exe and you should see a prompt asking for administrator privileges. This is needed to update the `/etc/hosts` file.


### MacOS

Currently running Distractifry on MacOS is done by issuing a command on the command line: `sudo ./distractifry`. You need `sudo` to be able to update the `/etc/hosts` file.

I am looking into a better way of running Distractifry with super user privileges on MacOS so you don't have to launch it from the command line. This work is going on in the `feature/macos-launch` branch. I am currently trying to figure out how to launch the application with escalated privileges, but not having much luck so far.

### Linux (Untested)

Linux has not been tested, but should work the same way MacOS does.


## Compiling

If you want to compile Distractifry on your own, you'll find instructions here.

Distractifry is tested and compiled with golang 1.15.3 in CI.

### Windows

Windows can be compiled on any platform, using the below command:

```
GOOS=windows CGO_ENABLED=0 go build -ldflags -H=windowsgui -o Distractifry.exe
```

### MacOS

MacOS must be compiled on a Mac. You should enable cgo.

```
CGO_ENABLED=1 go build -o Distractifry
```