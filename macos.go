// +build darwin

package main

import (
	"os"

	log "github.com/sirupsen/logrus"
)

const lineBreak = "\n"

func isRoot() bool {
	return os.Geteuid() == 0
}

func runAsRoot() {
	log.Panic("I need to be run with sudo somehow.. Help?!")
}

func getHostsPath() string {
	return "/etc/hosts"
}
