module gitlab.com/stokholm/distractifry

go 1.15

require (
	github.com/getlantern/systray v1.0.4
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/sys v0.0.0-20200515095857-1151b9dac4a9
)
