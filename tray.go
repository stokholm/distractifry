package main

import (
	"fmt"
	"syscall"

	"github.com/getlantern/systray"
	log "github.com/sirupsen/logrus"
)

func trayReady() {
	systray.SetIcon(frenchFries)
	systray.SetTooltip("Distractifry - Fry that distracting DNS")

	mBlockList := systray.AddMenuItem("Activate Block List", "Block DNS for your listed block domains")
	mUnblockList := systray.AddMenuItem("Deactivate Block List", "Unblock DNS for your listed block domains")
	mUnblockList.Hide()

	systray.AddSeparator()
	mInfo := systray.AddMenuItem("Info", "Application Information")
	mInfo.AddSubMenuItem(fmt.Sprintf("Version: %s", buildVersion), "The Application Version")
	mInfo.AddSubMenuItem(fmt.Sprintf("Built on: %s", buildTime), "When The Application Was Built")
	mQuit := systray.AddMenuItem("Quit", "Quit the whole app")

	go func() {
		for {
			select {
			case <-mQuit.ClickedCh:
				systray.Quit()
			case <-mBlockList.ClickedCh:
				log.Info("Activate block list")
				switchBlocking(true)
				mBlockList.Hide()
				mUnblockList.Show()
			case <-mUnblockList.ClickedCh:
				log.Info("Deactivate block list")
				switchBlocking(false)
				mBlockList.Show()
				mUnblockList.Hide()
			}
		}

	}()
}

func trayExit() {
	QuitChan <- syscall.SIGINT
}
